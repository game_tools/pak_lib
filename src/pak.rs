use std::borrow::BorrowMut;

pub trait Package
where
    Self: Sized,
{
    type D: DirectoryEntry;

    fn get_file_path(&self) -> &str;
    fn get_directory(&self) -> &Self::D;
    fn load(file_path: String) -> std::io::Result<Self>;
    fn unpack(&self, output_directory: &str) -> std::io::Result<()>;
    fn pack(directory: &str) -> std::io::Result<()>;

    fn print(&self) {
        let mut stats = print::Statistics::new();
        let directory = self.get_directory();
        directory.get_statistics(0, &mut stats);

        directory.print("", "", &stats.widths);

        stats.print();
    }
}

pub enum Node<D: DirectoryEntry> {
    Directory(D),
    File(D::F),
}

pub trait DirectoryEntry
where
    Self: Sized,
{
    type F: FileEntry;

    fn new(name: String) -> Self;
    fn get_directory_name(&self) -> &str;
    fn get_children(&self) -> &Vec<Node<Self>>;
    fn get_children_mut(&mut self) -> &mut Vec<Node<Self>>;

    fn unpack(
        &self,
        base_directory: &str,
        unpack_state: &mut <Self::F as FileEntry>::UnpackState,
    ) -> std::io::Result<()> {
        std::fs::create_dir(base_directory)?;
        for child in self.get_children() {
            match child {
                Node::Directory(directory) => {
                    let mut next_path = std::path::PathBuf::from(base_directory);
                    next_path.push(directory.get_directory_name());
                    directory.unpack(next_path.to_str().unwrap(), unpack_state)?;
                }
                Node::File(file) => {
                    file.unpack(base_directory, unpack_state)?;
                }
            }
        }
        return Ok(());
    }

    fn print(&self, indent: &str, child_indent: &str, widths: &print::ColumnWidths) {
        println!("{}{}", indent, self.get_directory_name());
        let children = self.get_children();
        if children.len() == 0 {
            return;
        }
        if children.len() > 1 {
            for child in children.split_last().unwrap().1 {
                match child {
                    Node::Directory(directory) => directory.print(
                        (child_indent.to_string() + "├─").as_str(),
                        (child_indent.to_string() + "│ ").as_str(),
                        widths,
                    ),
                    Node::File(file) => {
                        file.print((child_indent.to_string() + "├─").as_str(), widths)
                    }
                }
            }
        }
        match children.last().unwrap() {
            Node::Directory(directory) => directory.print(
                (child_indent.to_string() + "└─").as_str(),
                (child_indent.to_string() + "  ").as_str(),
                widths,
            ),
            Node::File(file) => file.print((child_indent.to_string() + "└─").as_str(), widths),
        }
    }

    fn get_statistics(&self, depth: usize, stats: &mut print::Statistics) {
        let name_width = self.get_directory_name().len() + 2 * depth;
        if name_width > stats.widths.name {
            stats.widths.name = name_width;
        }
        for child in self.get_children() {
            match child {
                Node::Directory(directory) => directory.get_statistics(depth + 1, stats),
                Node::File(file) => file.get_statistics(depth + 1, stats),
            }
        }

        stats.directory_count += 1;
    }

    fn find_or_create_path(&mut self, path: &[&str]) -> &mut Self {
        if path.len() == 0 {
            return self;
        }
        let part = path[0];
        return self
            .find_or_create_child(part)
            .find_or_create_path(&path[1..]);
    }

    fn find_or_create_child(&mut self, child_name: &str) -> &mut Self {
        return match self.find_child_dir(child_name) {
            Some(i) => {
                if let Node::Directory(return_directory) = self.get_children_mut()[i].borrow_mut() {
                    return_directory
                } else {
                    unreachable!()
                }
            }
            None => self.create_child_dir(child_name),
        };
    }

    fn find_child_dir(&self, child_name: &str) -> Option<usize> {
        for (i, child) in self.get_children().iter().enumerate() {
            if let Node::Directory(check_directory) = child {
                if check_directory.get_directory_name() == child_name {
                    return Some(i);
                }
            }
        }
        return None;
    }

    fn create_child_dir(&mut self, child_name: &str) -> &mut Self {
        self.get_children_mut()
            .push(Node::Directory(Self::new(child_name.to_string())));
        if let Node::Directory(return_directory) = self.get_children_mut().last_mut().unwrap() {
            return return_directory;
        } else {
            unreachable!();
        }
    }
}

pub trait FileEntry {
    type UnpackState;

    fn get_file_name(&self) -> &str;
    fn get_archive_index(&self) -> u8;
    fn get_offset(&self) -> u64;
    fn get_compressed_size(&self) -> Option<u64>;
    fn get_uncompressed_size(&self) -> Option<u64>;
    fn unpack(
        &self,
        base_directory: &str,
        unpack_state: &mut Self::UnpackState,
    ) -> std::io::Result<()>;

    fn print(&self, indent: &str, widths: &print::ColumnWidths) {
        print::print_left(
            (indent.to_string() + self.get_file_name()).as_str(),
            widths.name,
        );
        print::print_right(
            self.get_archive_index().to_string().as_str(),
            widths.archive_index,
        );
        print::print_right(self.get_offset().to_string().as_str(), widths.offset);
        print!(" c:");
        match self.get_compressed_size() {
            Some(size) => print::print_right(size.to_string().as_str(), widths.compressed_size),
            None => print::print_right("NA", widths.compressed_size),
        }
        print!(" u:");
        match self.get_uncompressed_size() {
            Some(size) => print::print_right(size.to_string().as_str(), widths.uncompressed_size),
            None => print::print_right("NA", widths.uncompressed_size),
        }
        println!();
    }

    fn get_statistics(&self, depth: usize, stats: &mut print::Statistics) {
        let file_name = self.get_file_name();
        let name_width = file_name.len() + 2 * depth;
        if name_width > stats.widths.name {
            stats.widths.name = name_width;
        }
        let index_width = self.get_archive_index().to_string().len();
        if index_width > stats.widths.archive_index {
            stats.widths.archive_index = index_width;
        }
        let offset_width = self.get_offset().to_string().len();
        if offset_width > stats.widths.offset {
            stats.widths.offset = offset_width;
        }
        let compressed_width = match self.get_compressed_size() {
            Some(size) => size.to_string().len(),
            None => 2,
        };
        if compressed_width > stats.widths.compressed_size {
            stats.widths.compressed_size = compressed_width;
        }
        let uncompressed_width = match self.get_uncompressed_size() {
            Some(size) => size.to_string().len(),
            None => 2,
        };
        if uncompressed_width > stats.widths.uncompressed_size {
            stats.widths.uncompressed_size = uncompressed_width;
        }

        if let Some(compressed_size) = self.get_compressed_size() {
            stats.total_compressed_size += compressed_size;
        }
        if let Some(uncompressed_size) = self.get_uncompressed_size() {
            stats.total_uncompressed_size += uncompressed_size;
        }
        let extension = match file_name.rfind('.') {
            Some(pos) => &file_name[pos + 1..],
            None => "None",
        };
        let file_type = stats.types.entry(extension.to_string()).or_insert(0);
        *file_type += 1;
    }
}

mod print {
    use std::collections::HashMap;

    pub struct ColumnWidths {
        pub name: usize,
        pub archive_index: usize,
        pub offset: usize,
        pub compressed_size: usize,
        pub uncompressed_size: usize,
    }

    impl ColumnWidths {
        pub fn new() -> Self {
            return Self {
                name: 0,
                archive_index: 0,
                offset: 0,
                compressed_size: 0,
                uncompressed_size: 0,
            };
        }
    }

    pub struct Statistics {
        pub widths: ColumnWidths,
        pub types: HashMap<String, u32>,
        pub directory_count: u32,
        pub total_compressed_size: u64,
        pub total_uncompressed_size: u64,
    }

    impl Statistics {
        pub fn new() -> Self {
            return Self {
                widths: ColumnWidths::new(),
                types: HashMap::new(),
                directory_count: 0,
                total_compressed_size: 0,
                total_uncompressed_size: 0,
            };
        }

        pub fn print(&self) {
            println!();
            println!("{} compressed", self.total_compressed_size);
            println!("{} uncompressed", self.total_uncompressed_size);
            println!(
                "{}% compression ratio",
                self.total_compressed_size as f32 / self.total_uncompressed_size as f32 * 100.0
            );
            println!();
            println!("{} directories", self.directory_count);
            let mut total_files = 0;
            for (_, count) in &self.types {
                total_files += count;
            }
            println!("{} files", total_files);
            println!("{} file types", self.types.len());
            println!();
            let mut types: Vec<_> = self.types.iter().collect();
            types.sort();
            for file_type in &types {
                println!("{}: {} files", file_type.0, file_type.1);
            }
        }
    }

    pub fn print_right(text: &str, column_width: usize) {
        print!(
            "{} {}",
            " ".repeat(column_width - text.chars().count()),
            text
        );
    }

    pub fn print_left(text: &str, column_width: usize) {
        print!(
            "{} {}",
            text,
            " ".repeat(column_width - text.chars().count())
        );
    }
}

pub mod util {
    pub fn get_file_paths_to_pack(directory: &str) -> std::io::Result<Vec<String>> {
        let mut entries = Vec::new();
        get_directory_contents(directory, &mut entries)?;
        return Ok(entries);
    }

    fn get_directory_contents(directory: &str, entries: &mut Vec<String>) -> std::io::Result<()> {
        let directory_contents = std::fs::read_dir(directory)?;
        let mut contained_paths = Vec::new();
        for path in directory_contents {
            contained_paths.push(path?.path());
        }
        contained_paths.sort();
        for path in contained_paths {
            match path.is_file() {
                true => entries.push(path.to_str().unwrap().to_string()),
                false => get_directory_contents(path.to_str().unwrap(), entries)?,
            }
        }
        return Ok(());
    }
}

use crate::pak::Package;
use std::process::exit;

pub fn run_cli<T: Package>() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        println!("no arguments; try help");
        exit(1);
    }
    match args[1].as_str() {
        "list" => commands::list::<T>(args),
        "unpack" => commands::unpack::<T>(args),
        "pack" => commands::pack::<T>(args),
        "help" => commands::print_help(),
        _ => {
            println!("unrecognized command; try \"help\"");
            exit(1);
        }
    }
}

mod commands {
    use super::Package;

    pub fn print_help() {
        println!("Currently supported commands:\n list [pak file]\n unpack [pak file]");
    }

    pub fn unpack<T: Package>(args: Vec<String>) {
        require_2_args(args.len());
        let package = T::load(args[2].to_string()).expect("Failed to load package");
        let package_path = package.get_file_path();
        package
            .unpack(
                &package_path[..package_path
                    .rfind('.')
                    .expect("package file nam has no extension")],
            )
            .unwrap();
    }

    pub fn pack<T: Package>(args: Vec<String>) {
        require_2_args(args.len());
        T::pack(&args[2]).unwrap();
    }

    pub fn list<T: Package>(args: Vec<String>) {
        require_2_args(args.len());
        let package = T::load(args[2].to_string()).expect("Failed to load package");
        package.print();
    }

    fn require_2_args(num_args: usize) {
        if num_args < 3 {
            println!("missing file argument");
            super::exit(1);
        }
        if num_args > 3 {
            println!("unexpected argument");
            super::exit(1);
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

mod pak;
pub use pak::{DirectoryEntry, FileEntry, Node, Package, util};
pub mod cli;
